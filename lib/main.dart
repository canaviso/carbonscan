/* Sustainability Hackathon
Carbon Scanner v.0.1 Alpha by fsociety

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*ToDo    In lib/previewscreen/preview_screen.dart
*ToDo     1. Pass image details to Google Cloud Vision API service to identify item
*ToDo      - Google Cloud Vision will identify item
*ToDo  2. Save item details to Firebase DB.
*ToDo      - Firebase project created, app is registered.
*ToDo      - Refer to android/app/google-services.json
*ToDo  3. Use https://www.2030calculator.com/ to get CO2 details of scanned item
*ToDo      - This is a paid service.
*ToDo      - Might need to check for alternatives
*ToDo
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

*/



import 'dart:async';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:bottom_nav_layout/bottom_nav_layout.dart';
import 'package:carbon_scan/camerascreen/camera_screen.dart';
import 'package:firebase_database/firebase_database.dart';

//import 'dart:io';
//import 'package:camera/camera.dart';
//import 'package:carbon_scan/previewscreen/preview_screen.dart';
//import 'package:firebase_core/firebase_core.dart';
//import 'package:firebase_ml_vision/firebase_ml_vision.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
//import 'package:uuid/uuid.dart';


Future<void> main() async {
  // WidgetsFlutterBinding.ensureInitialized();
  // final cameras = await availableCameras();
  // final firstCamera = cameras.first;

  runApp(MaterialApp(
    home: BottomNavLayout(
      // The app's destinations
      pages: [
        (_) => LogHome(),
        (_) => DataList(),
        (_) => CameraApp(),
        (_) => ListTips(),
        (_) => ProfilePage(),
      ],
      bottomNavigationBar: (currentIndex, onTap) =>
          BottomNavigationBar(
            currentIndex: currentIndex,
            onTap: (index) => onTap(index),
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home', backgroundColor: Colors.blue),
              BottomNavigationBarItem(icon: Icon(Icons.history), label: 'History', backgroundColor: Colors.blue),
              BottomNavigationBarItem(icon: Icon(Icons.photo_camera), label: 'Scan Item', backgroundColor: Colors.blue),
              BottomNavigationBarItem(icon: Icon(Icons.lightbulb), label: 'Tips', backgroundColor: Colors.blue),
              BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile', backgroundColor: Colors.blue),
            ],
          ),
    ),
  ));
}

class CameraApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: CameraScreen(),
    );
  }
}

class LogHome extends StatefulWidget {

  @override
  _LogHomeState createState() => _LogHomeState();
}

class _LogHomeState extends State<LogHome> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          body: Padding(
            padding: EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10),
                    child: Image.asset('assets/carbon_scan_logo.png'),
                ),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10),
                    child:  Text(
                          "Scan everyday items to find out their carbon footprint!",
                           textAlign: TextAlign.center,
                           style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'User Name',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                    ),
                  ),
                ),
                TextButton(
                  onPressed: (){
                    //forgot password screen
                  },
                  //textColor: Colors.blue,
                  child: Text('Forgot Password'),
                ),
                Container(
                    height: 50,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ElevatedButton(
                      //textColor: Colors.white,
                      //color: Colors.blue,
                      child: Text('Login'),
                      onPressed: () {
                        print(nameController.text);
                        print(passwordController.text);
                      },
                    )),
                Container(
                    child: Row(
                      children: <Widget>[
                        Text('Does not have account?'),
                        TextButton(
                          //textColor: Colors.blue,
                          child: Text(
                            'Sign in',
                            style: TextStyle(fontSize: 20),
                          ),
                          onPressed: () {
                            //signup screen
                          },
                        )
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                    ))
              ],
            )));
  }
}


class DataList extends StatefulWidget {
  @override
  _DataListState createState() => _DataListState();
}

class _DataListState extends State<DataList> {
  //final textcontroller = TextEditingController();
  //final databaseRef = FirebaseDatabase.instance.reference();  //.child("products");
  //final Future<FirebaseApp> _future = Firebase.initializeApp();
  //var lists = new List();
  final dbRef = FirebaseDatabase.instance.reference().child("products");

  List<Map<dynamic, dynamic>> lists = [];



  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Scanned List"),
        ),
        body: FutureBuilder(
            future: dbRef.once(),
            builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
              if (snapshot.hasData && snapshot.data.toString() != '[]') {
                lists.clear();
                Map<dynamic, dynamic> values = snapshot.data.value; //snapshot.data.value;
                //print("Values: $values");
                values?.forEach((key, values) {
                     lists.add(values);
                });
                return new ListView.builder(
                    shrinkWrap: true,
                    itemCount: lists.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                                               Text("Date: " + lists[index]["Date"],style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
                                               Text("Item: " + lists[index]["Item"],style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.normal)),
                                               Text("CO2 Footprint: " + lists[index]["CO2"],style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.normal)),
                                             ],

                        ),
                      );
                    });
              }
              return CircularProgressIndicator();
            }));
  }
  
}


class ListTips extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tips',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(title: Text('Tips to reduce your CO\u00B2 footprint')),
        body: BodyLayout(),
      ),
    );
  }
}

class BodyLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _myListView(context);
  }
}

Widget _myListView(BuildContext context) {

  // backing data
  final europeanCountries = [
    '1. Insulate your home.',
    '2. Switch to renewable energy.',
    '3. Buy energy efficient products.',
    '4. Use less water.',
    '5. Turn off the lights when not in use.',
    '6. Cycle to work if possible.',
    '7. Eliminate single-use plastic.',
    '8. Switch off computers.',
    '9. Think Green.',
    '10. Reduce, Reuse, Recycle.'];

  return ListView.builder(
    itemCount: europeanCountries.length,
    itemBuilder: (context, index) {
      return ListTile(
        leading: Icon(Icons.lightbulb,
                      color: Colors.yellow),
        title: Text(europeanCountries[index]),
      );
    },
  );

}

class ProfilePage extends StatefulWidget {

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
      ),
      body: Column(
        children: [
          Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colors.blueAccent, Colors.blue]
                  )
              ),
              child: Container(
                width: double.infinity,
                height: 400.0,
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        backgroundImage: AssetImage('assets/fsociety_profile.jpg'),
                        radius: 50.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        "fsociety",
                        style: TextStyle(
                          fontSize: 22.0,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Card(
                        margin: EdgeInsets.symmetric(horizontal: 20.0,vertical: 5.0),
                        clipBehavior: Clip.antiAlias,
                        color: Colors.white,
                        elevation: 5.0,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 22.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: Column(

                                  children: [
                                    Text(
                                      "Shares",
                                      style: TextStyle(
                                        color: Colors.blueAccent,
                                        fontSize: 22.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Text(
                                      "5",
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.blueAccent,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(

                                  children: [
                                    Text(
                                      "CO\u00B2 footprint",
                                      style: TextStyle(
                                        color: Colors.blueAccent,
                                        fontSize: 22.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Text(
                                      "1300g",
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.red,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
          ),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(10),
            child: Image.asset('assets/social.jpg'),

          ),
          Container(),
        ],
      ),
    );
  }
}
