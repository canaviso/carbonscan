/*
 * Copyright (c) 2019 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
*  Credits to Razeware LLC for allowing to use code snippets
* TODO * * * * * * * * * * * * * *   TODO   * * * * * * * * * * * * * * * *
*  1. Pass image details to Google Cloud Vision to identify item
*  2. Save item photo details to Firebase DB.
*      - Firebase project created, item text are uploaded.
*      - Refer to android/app/google-services.json
*  3. Use https://www.2030calculator.com/ to get CO2 details of scanned item
*      - This is a paid service.
*      - Might need to check for alternatives
* TODO * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
import 'dart:io';
import 'dart:typed_data';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:intl/intl.dart';
class PreviewImageScreen extends StatefulWidget {
  final String imagePath;

  //TODO - Call GoogleVision API by passing imagepath
  //TODO - Link with CO2 footprint DB to get item CO2 footprint
  //TODO - Pass both details to onPressed event and upload to Firebase
  PreviewImageScreen({this.imagePath});

  @override
  _PreviewImageScreenState createState() => _PreviewImageScreenState();
}



class _PreviewImageScreenState extends State<PreviewImageScreen> {
  final dbRef = FirebaseDatabase.instance.reference().child("products");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Carbon Footprint Details'),
        backgroundColor: Colors.blueGrey,
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
                flex: 2,
                child: Image.file(File(widget.imagePath), fit: BoxFit.cover)),
            SizedBox(height: 20.10),
            textSection,
            Flexible(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(10.0),
                child: ElevatedButton(
                  onPressed: () {
                    DateTime now = DateTime.now();
                    String formattedDate = DateFormat('yyyyMMdd-HH:mm').format(now);
                    print("Current:$formattedDate");
                    dbRef.push().set({
                      "Date": formattedDate,
                      "Item": "plastic bottle",
                      "CO2": "83g" });
                   // getBytesFromFile().then((bytes) {
                   //   Share.file('Share via:', basename(widget.imagePath),
                   //       bytes.buffer.asUint8List(), 'image/png');
                   //  });
                  },
                  child: Text('Save'),
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(10.0),
                child: ElevatedButton(
                  onPressed: () {
                    getBytesFromFile().then((bytes) {
                      Share.file('Share via:', basename(widget.imagePath),
                          bytes.buffer.asUint8List(), 'image/png');
                    });
                  },
                  child: Text('Share'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Future Changes
  //
  Widget textSection = const Padding(
    padding: EdgeInsets.all(32),
    child: Text("Plastic Bottle is about 83g of CO2 footprint",
        textAlign: TextAlign.center,
        style:
        TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold)),

  );


  Future<ByteData> getBytesFromFile() async {
    Uint8List bytes = File(widget.imagePath).readAsBytesSync() as Uint8List;
    return ByteData.view(bytes.buffer);
  }
}

//Clark backup code
/*
class _PreviewImageScreenState extends State<PreviewImageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Carbon Footprint Details'),
        backgroundColor: Colors.blueGrey,
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
                flex: 2,
                child: Image.file(File(widget.imagePath), fit: BoxFit.cover)),
            SizedBox(height: 20.10),
            Flexible(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(60.0),
                child: ElevatedButton(
                  onPressed: () {
                    getBytesFromFile().then((bytes) {
                      Share.file('Share via:', basename(widget.imagePath),
                          bytes.buffer.asUint8List(), 'image/png');
                    });
                  },
                  child: Text('Share'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<ByteData> getBytesFromFile() async {
    Uint8List bytes = File(widget.imagePath).readAsBytesSync() as Uint8List;
    return ByteData.view(bytes.buffer);
  }
}
*/
